document.getElementById('btnBuscar').addEventListener('click', buscarCocktail);
    document.getElementById('btnLimpiar').addEventListener('click', limpiar);
    function mostrarError(mensaje) {
      // const mensajeError = document.getElementById('mensajeErrorTexto');
      // mensajeError.textContent = mensaje;
      // document.getElementById('mensajeError').style.display = 'block';
    }

    function buscarCocktail() {
      const nombreCocktail = document.getElementById('txtNombre').value;

      // Hacer la solicitud a la API con Axios
      axios.get(`https://www.thecocktaildb.com/api/json/v1/1/search.php?s=${nombreCocktail}`)
        .then(response => {
          const cocktails = response.data.drinks;
          if (cocktails) {
            const cocktail = cocktails.find(c => c.strDrink === nombreCocktail);
            console.log(cocktail)
            if (cocktail) {
              document.getElementById('categoria').innerHTML = `<h3>Categoría: ${cocktail.strCategory}</h3> `;
              document.getElementById('instrucciones').innerHTML =`<h3>Instrucciones: ${cocktail.strInstructions}</h3>`;
              document.getElementById('imagen').innerHTML = `<img src="${cocktail.strDrinkThumb}" alt="${nombreCocktail}">`;
            } else {
              document.getElementById('categoria').innerHTML = `<h3>No encontro </h3> `;
              
            }
          } else {
            mostrarError(`No se encontró el cóctel con el nombre '${nombreCocktail}'.`);
          }
        })
        .catch(error => {
          mostrarError('Error al buscar el cóctel. Por favor, inténtalo de nuevo.');
          document.getElementById('categoria').innerHTML = '';
          document.getElementById('instrucciones').innerHTML = '';
          document.getElementById('imagen').innerHTML = '';
          document.getElementById('categoria').innerHTML = `<h3>No encontro </h3> `;
          console.error('Error al buscar el cóctel:', error);
        });
    }

    function limpiar() {
      document.getElementById('categoria').innerHTML = '';
      document.getElementById('instrucciones').innerHTML = '';
      document.getElementById('imagen').innerHTML = '';
      document.getElementById('txtNombre').value = '';
    }